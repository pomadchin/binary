# Binary

Repo with built binaries for public usage.

## Where these binaries used

* [GeoDocker cluster](github.com/geotrellis/geodocker-cluster)

## License

* Licensed under the Apache License, Version 2.0: http://www.apache.org/licenses/LICENSE-2.0
